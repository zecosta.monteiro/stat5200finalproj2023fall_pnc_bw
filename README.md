# STAT5200 - Fall 2023 - Final Project: The impact of prenatal care timing on birth weight in Brazil

This repository contains the replication materials for the estimation of the effect of prenatal care timing on birth weight in the state of São Paulo, Brazil (2015-2019).

The raw microdata can be found in [the DATASUS webpage](https://datasus.saude.gov.br/transferencia-de-arquivos/), administered by the Brazilian Ministry of Health.

## R Codes

### `read_sinasc_sp.R`

Use this code to read, clean and prepare the data set for the analysis.

### `table1.R`

Use this code to construct the paper table 1 (descriptive statistics).

### `ivmodel_sinasc.R`

Use this code to run the analysis - OLS and the 2SLS regressions.
